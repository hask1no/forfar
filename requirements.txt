Django==4.0.1
psycopg2-binary==2.9.3
redis
# wkhtmltopdf
django-cors-headers
requests~=2.27.1
pdfkit~=1.0.0
python-decouple
