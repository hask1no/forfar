# Сервис для генерации чеков.

Создание PDF файла с использованием [wkhtmltopdf](https://hub.docker.com/r/openlabs/docker-wkhtmltopdf-aas/) (Docker)

### Стэк
- Python 3.9
- Django 4.0.1
- PostgreSQL 
- Docker