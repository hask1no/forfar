import requests

from rq import get_current_job
from django_rq import job


@job
def count_words(url):
    resp = requests.get(url)
    return len(resp.text.split())
