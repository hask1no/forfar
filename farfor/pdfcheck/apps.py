from django.apps import AppConfig


class PdfcheckConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pdfcheck'
