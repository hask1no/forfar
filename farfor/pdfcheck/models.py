# Создание БД
from django.db import models


class Printer(models.Model):
    name = models.CharField('название принтера', max_length=100)
    api_key = models.CharField('ключ доступа к API', max_length=250)
    check_type = models.CharField('тип чека которые печатает принтер', max_length=250)
    point_id = models.IntegerField('точка к которой привязан принтер')

    def __str__(self):
        return '%s %s %s' % (self.name, self.check_type, self.point_id)

    class Meta:
        verbose_name = 'Принтер'
        verbose_name_plural = 'Принтеры'


class Check(models.Model):
    printer_id = models.ForeignKey(Printer, on_delete=models.CASCADE, verbose_name='принтер')
    type = models.CharField('тип чека', max_length=100)
    order = models.JSONField('информация о заказе')
    status = models.CharField('статус чека', max_length=100)
    pdf_file = models.FileField('ссылка на созданный PDF-файл', max_length=250)

    def __str__(self):
        return '%s %s %s' % (self.type, self.order, self.status)

    class Meta:
        verbose_name = 'Чек'
        verbose_name_plural = 'Чеки'
