import time
from rq import Queue
from redis import Redis
from farfor.pdfcheck.tasks import count_words
import tasks


# Соединение с redis
redis_con = Redis()
queue = Queue(connection=redis_con)


# выполнение задачи в очередь
def queue_tasks():
    job = queue.enqueue(count_words, 'http://localhost:8000/')
    print(job.result)


def main():
    queue_tasks()


if __name__ == "__main__":
    main()
