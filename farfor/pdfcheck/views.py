import base64
import requests
import json

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.template.loader import get_template
from django.template import Context
from .models import Check, Printer

# async tasks
from rq import get_current_job
from django_rq import job


def main_page(request):
    return render(request, 'pdfcheck/main.html')


@job
def create_checks(request):
    """
    Запись в БД. Считать данные с таблицы и проверить чек (есть или нет) Провекра по type, id order, printer_id
    """
    if request.method == 'POST':
        j = request.body.decode(encoding='utf-8')
        data_for_check = json.loads(j)
        printer_filter = Printer.objects.filter(point_id=data_for_check['point_id']).values('id', 'check_type')
        for x in printer_filter:
            check_type = x['check_type']
            pdf_file = f"{data_for_check['id']}_{x['check_type']}.pdf"
            if Check.objects.filter(type=x['check_type'], order__id=data_for_check['id'],
                                    printer_id_id=x['id']).exists():
                return HttpResponse(status=400)
            else:
                Check(type=x['check_type'], order=data_for_check, status='new',
                      pdf_file=pdf_file, printer_id_id=x['id']).save()
                create_pdf(data_for_check, check_type, pdf_file)
        return HttpResponse(status=200)
    else:
        return HttpResponse(status=404)


@job
def create_pdf(data_for_check, check_type, pdf_file):
    url = 'http://localhost:5431/'

    template = get_template(f'pdfcheck/{check_type}_check.html')
    html = template.render(data_for_check)

    byte_content = bytes(html, 'utf-8')
    base64_string = base64.b64encode(byte_content).decode('utf-8')

    data = {'contents': base64_string}
    headers = {'Content-Type': 'application/json'}

    response = requests.post(url, data=json.dumps(data), headers=headers)

    # Save the response contents to a file
    with open(f'pdfcheck/media/pdf/{pdf_file}', 'wb') as f:
        f.write(response.content)


def new_checks(request):
    if request.method == 'GET':
        pass
    else:
        return HttpResponse(status=404)


def check(request):
    if request.method == 'GET':
        pass
    else:
        return HttpResponse(status=404)
