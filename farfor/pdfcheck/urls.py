from django.urls import path
from . import views


urlpatterns = [
    path('create_checks/', views.create_checks, name='create_check'),
    path('new_checks/', views.new_checks, name='new_check'),
    path('check/', views.check, name='check'),
    path('', views.main_page),
]
